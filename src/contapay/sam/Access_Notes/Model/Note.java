package contapay.sam.Access_Notes.Model;

import java.io.Serializable;

/**
 * Created by Sam on 10/1/13.
 */
public class Note implements Serializable {
    private long created_at;
    private String subject;
    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Constructor that initializes the note object and adds a timestamp
     *
     * @param subject
     * @param note
     */
    public Note(String subject, String note)
    {
        this.subject = subject;
        this.note = note;
        this.created_at = System.currentTimeMillis();

        if ( this.subject.isEmpty() )
            this.subject = "Untitled";
    }

    public Note()
    {
        this.subject = "Untitled";
        this.created_at = System.currentTimeMillis();
    }

    public long getCreated_at()
    {
        return this.created_at;
    }
}
