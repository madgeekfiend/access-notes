package contapay.sam.Access_Notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import contapay.sam.Access_Notes.Model.Note;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Sam on 10/1/13.
 */
public class NotesAdapter extends ArrayAdapter<Note> {
    private List<Note> notes;
    private Context context;

    public NotesAdapter(Context context, List<Note> notes)
    {
        super(context,R.layout.note_detail_row,notes);
        this.notes = notes;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.note_detail_row,parent,false);

        TextView noteTitle = (TextView)rowView.findViewById(R.id.text_view_subject);
        TextView noteTime = (TextView)rowView.findViewById(R.id.text_view_time);

        Note note = notes.get(position);
        // Now I got this
        noteTitle.setText(note.getSubject());
        Date date = new Date(note.getCreated_at());
        noteTime.setText(new SimpleDateFormat("MM/dd/yyyy HH:mm").format(date));

        return rowView;
    }
}
