package contapay.sam.Access_Notes;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import contapay.sam.Access_Notes.Model.Note;

/**
 * Created by Sam on 10/1/13.
 */
public class NoteActivity extends ActionBarActivity {

    private Note note;
    private int NoteIndex = -1;
    public static final String NOTE_KEY="contapay.sam.keys.NOTE";
    public static final String NOTE_INDEX_KEY="contapay.sam.keys.NOTE_INDEX";
    public final static int NOTE_RESULT_SAVE_EDIT = 1000;
    public final static int NOTE_RESULT_CANCEL = 2000;
    public final static int NOTE_RESULT_SAVE_NEW = 3000;

    public void setNote(Note note)
    {
        this.note = note;
    }

    @SuppressLint("NewApi")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.note_activity);

        // See if we got a note
        note = (Note)getIntent().getSerializableExtra(NOTE_KEY);
        NoteIndex = getIntent().getIntExtra(NOTE_INDEX_KEY,-1);
        // Add text field to ActionBar

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setCustomView(R.layout.note_actionbar);
            EditText title = (EditText)actionBar.getCustomView().findViewById(R.id.notes_edit);
            title.setOnEditorActionListener( new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    String title = v.getText().toString();
                    note.setSubject(title);
                    return true;
                }
            });
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME);
            if (note == null )
            {
                note = new Note();
                title.setText(note.getSubject());
            }
            else
                title.setText(note.getSubject());
        }

        // Set focus to text view
        EditText noteField = (EditText)findViewById(R.id.editText);
        if ( note != null && NoteIndex > -1 )
            noteField.setText(note.getNote());
        noteField.requestFocus();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.action_note_cancel:
                setResult(NOTE_RESULT_CANCEL);
                finish();
                return true;
            case R.id.action_note_save:
                // Save note to intent
                Intent intent = new Intent();
                onSave();
                intent.putExtra(NOTE_KEY,note);
                intent.putExtra(NOTE_INDEX_KEY,NoteIndex);
                setResult((NoteIndex > -1)?NOTE_RESULT_SAVE_EDIT:NOTE_RESULT_SAVE_NEW,intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.note_action_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void onSave()
    {
        EditText txt = (EditText)findViewById(R.id.editText);
        EditText title = (EditText)getActionBar().getCustomView().findViewById(R.id.notes_edit);
        note.setSubject(title.getText().toString());
        note.setNote(txt.getText().toString());
    }
}