package contapay.sam.Access_Notes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import contapay.sam.Access_Notes.Model.Note;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class NotesListActivity extends ActionBarActivity {

    private List<Note> notes = new ArrayList<Note>();
    private final String FILENAME = "notes_archive";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        ListView lv = (ListView)findViewById(R.id.list_detail_view);
        //lv.setClickable(false);
        lv.setItemsCanFocus(false);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Start a new activity with the existing note and wait for a result
                Intent intent = new Intent(NotesListActivity.this,NoteActivity.class);
                Note note = notes.get(position);
                intent.putExtra(NoteActivity.NOTE_KEY,note);
                intent.putExtra(NoteActivity.NOTE_INDEX_KEY,position);
                NotesListActivity.this.startActivityForResult(intent,position);

            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.list_detail_view);
        {
            // Build out the context menu
            MenuInflater inflater = getMenuInflater();
            menu.setHeaderTitle("Select Action");
            inflater.inflate(R.menu.list_row_context,menu);

        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
       // return super.onContextItemSelected(item);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        //  info.position will give the index of selected item
        int IndexSelected=info.position;

        if (item.getTitle().toString().equals("Delete"))
        {
            // Delete this item
            if ( notes == null )
                NotesSerializeFromFile();
            notes.remove(IndexSelected);
            saveToFile();
        }

        refreshList();

        return true;
//        item.get
//
//        switch (id)
//        {
//            case R.id.list_detail_view:
//                Log.d("NotesListActivity","This is a test");
//                break;
//            default:
//                break;
//        }

      //  return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Serialize to File and JSON
        // Assume it's 0 and just erase the file
        if ( notes != null && notes.size() > 0 )
            saveToFile();
        else
            deleteFile(FILENAME);   // Just delete the file because the list is empty let's just make the file gone

    }

    @Override
    protected void onResume() {
        super.onResume();
        NotesSerializeFromFile();
        // Add the things I need to this
        refreshList();
    }

    private void NotesSerializeFromFile()
    {
        // Read from file and deserialize to notes
        String fileContent = readFromFile();
        Gson gson = new Gson();
        Type collectionType = new TypeToken<List<Note>>(){}.getType();
        notes = gson.fromJson(fileContent.toString(), collectionType);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_listview_action_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_search:
                // Let's spin up a new activity and add stuff
                Intent intent = new Intent(this,NoteActivity.class);
                startActivityForResult(intent,1000);
                //startActivity(intent);
                return true;
            case R.id.action_clear_all:
                if (notes != null)
                    notes.clear();
                deleteFile(FILENAME);
                refreshList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (null == data) return;
        Note note = (Note)data.getSerializableExtra(NoteActivity.NOTE_KEY);
        int noteIndex = data.getIntExtra(NoteActivity.NOTE_INDEX_KEY,-1);

        switch(resultCode)
        {
            case NoteActivity.NOTE_RESULT_SAVE_NEW:
                if ( notes == null )
                    notes = new ArrayList<Note>();
                notes.add(note);
                saveToFile();
                break;
            case NoteActivity.NOTE_RESULT_SAVE_EDIT:
                if ( notes == null )
                    NotesSerializeFromFile();
                notes.set(noteIndex,note);
                saveToFile();
                break;
            case NoteActivity.NOTE_RESULT_CANCEL:
                break;
        }

        refreshList();
    }

    private void refreshList()
    {
        ListView lv = (ListView)findViewById(R.id.list_detail_view);

        if ( notes != null )
            lv.setAdapter(new NotesAdapter(this, notes));
        else
            lv.setAdapter(null);

        registerForContextMenu(lv);

    }

    private void saveToFile()
    {
        Gson gson = new Gson();
        String json = gson.toJson(notes);

        // Write string to file
        try {
            deleteFile(FILENAME);
            FileOutputStream fos = openFileOutput(FILENAME,Context.MODE_PRIVATE);
            fos.write(json.getBytes());
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e(getLocalClassName(), "File not found to serialize to", e);
        } catch (IOException e) {
            Log.e(getLocalClassName(), "Unable to write to file", e);
        }
    }

    // I got this from
    // http://stackoverflow.com/questions/14376807/how-to-read-write-string-from-a-file-in-android
    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput(FILENAME);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}
